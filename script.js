//add users
db.users.insertMany(
	[
		{
			"firstName": "Diane",
			"lastName": "Murphy",
			"email": "dmuphy@mail.com",
			"isAdmin": "no",
			"isActive": "yes"
		},
		{
			"firstName": "Mary",
			"lastName": "Patterson",
			"email": "mpatterson@mail.com",
			"isAdmin": "no",
			"isActive": "yes"
		},
		{
			"firstName": "Jeff",
			"lastName": "Firrelli",
			"email": "jfirrelli@mail.com",
			"isAdmin": "no",
			"isActive": "yes"
		},
		{
			"firstName": "Gerard",
			"lastName": "Bondur",
			"email": "gbondur@mail.com",
			"isAdmin": "no",
			"isActive": "yes"
		},
		{
			"firstName": "Pamela",
			"lastName": "Castillo",
			"email": "pcastillo@mail.com",
			"isAdmin": "yes",
			"isActive": "no"
		},
		{
			"firstName": "George",
			"lastName": "Vanauf",
			"email": "gvanauf@mail.com",
			"isAdmin": "yes",
			"isActive": "yes"
		}
	]
);


//add courses
db.courses.insertMany(
	[
		{
			"name": "Professional Development",
			"price": "10,000.00"
		},
		{
			"name": "Business Processing",
			"price": "13,000.00"
		}
	]
);


//courses added using CRUD
db.courses.updateOne(
		{
			"name": "Professional Development"
		},
		{
			$set: { "enrollees": "Murphy, Firrelli"}
		}
	);



db.courses.updateOne(
		{
			"name": "Business Processing"
		},
		{
			$set:{"enrollees": "Bondur, Patterson"}
		}
	);


//users who are not admin
db.users.find({"isAdmin": "no"});